[DEPRECATED]
# NFDI4Earth Ontology

NOTE: This project is deprecated, the NFDI4Earth ontology is being continued in the [LinkML project](https://git.rwth-aachen.de/nfdi4earth/knowledgehub/nfdi4earth-kh-schema). See the human-readable [ontology documentation](https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/).

## Editing
Edit by loading the .owl file into [Protégé](https://protege.stanford.edu).

## Publication
Publish via the [widoco tool](https://github.com/dgarijo/Widoco)

### Live

[https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-ontology/](https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-ontology/)

### Local

#### Installation
Download the latest release and possible for another JRE version from https://github.com/dgarijo/Widoco/releases
```bash
# requires Java 11, possibly update version
wget https://github.com/dgarijo/Widoco/releases/download/v1.4.17/java-11-widoco-1.4.17-jar-with-dependencies.jar
```

#### Run the tool
```bash
java -jar java-11-widoco-1.4.17-jar-with-dependencies.jar -ontFile n4e.owl -getOntologyMetadata -webVowl -outFolder n4e
```
