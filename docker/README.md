# What?

This is the basic Dockerfile for the docker container, that is used to
build the [view on the ontology](https://nfdi4earth.pages.rwth-aachen.de/metadatastandardsandcommons/nfdi4earth-ontology/)
via gitlab.pages.

# How?

3 major steps have to be done:

1. init upload
2. build image
3. push image to gitlab registry

## Login to gitlab registry

```bash
docker login registry.git.rwth-aachen.de -u ${tokenname} -p ${token}}
```

## Create image name

```bash
version=$(grep 'version=' Dockerfile | cut -d"=" -f2 | cut -d"\"" -f2)
iname="registry.git.rwth-aachen.de/nfdi4earth/metadatastandardsandcommons/nfdi4earth-ontology:${version}"
```

## Build image

```bash
docker build -t ${iname} .
```

## Build image

```bash
docker push ${iname}
```
